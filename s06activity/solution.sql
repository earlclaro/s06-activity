


-- Run the MySQL/Maria DB in Terminal
mysql -u root -p

-- Show/retrieve all the databases:
SHOW DATABASES;

-- Create/add a database:
-- Syntax: CREATE DATABASE database_name;
CREATE DATABASE s06act;

CREATE TABLE Author (
  Au_id VARCHAR(11) PRIMARY KEY,
  Au_lname VARCHAR(40),
  Au_fname VARCHAR(20),
  address VARCHAR(40),
  city VARCHAR(20),
  state CHAR(2)
);

CREATE TABLE Publisher (
  Pub_id VARCHAR(4) PRIMARY KEY,
  Pub_name VARCHAR(50) NOT NULL,
  city VARCHAR(50) NOT NULL
);

CREATE TABLE Title (
  Title_id VARCHAR(10) PRIMARY KEY,
  title VARCHAR(100),
  type VARCHAR(20),
  price DECIMAL(6,2),
  Pub_id VARCHAR(4),
  FOREIGN KEY (Pub_id) REFERENCES Publisher(Pub_id)
);

CREATE TABLE Author_title (
  Au_id VARCHAR(11) NOT NULL,
  Title_id VARCHAR(10) NOT NULL,
  PRIMARY KEY (Au_id, Title_id),
  FOREIGN KEY (Au_id) REFERENCES Author(Au_id),
  FOREIGN KEY (Title_id) REFERENCES Title(Title_id)
);




INSERT INTO Author (Au_id, Au_lname, Au_fname, address, city, state) VALUES 
('172-32-1176', 'White', 'Johnson', '10932 Bigge Rd.', 'Menlo Park', 'CA'),
('213-46-8915', 'Green', 'Marjorie', '309 63rd St. #411', 'Oakland', 'CA'),
('238-95-7766', 'Carson', 'Cheryl', '589 Darwin Ln.', 'Berkeley', 'CA'),
('267-41-2394', 'O’Leary', 'Michael', '22 Cleveland Av. #14', 'San Jose', 'CA'),
('274-80-9391', 'Straight', 'Dean', '5420 College Av. 10', 'Oakland', 'CA'),
('341-22-1782', 'Smith', 'Meander', '10 Mississippi Dr.', 'Lawrence', 'KS'),
('409-56-7008', 'Bennet', 'Abraham', '6223 Bateman St.', 'Berkeley', 'CA'),
('427-17-2319', 'Dull', 'Ann', '3410 Blonde St.', 'Palo Alto', 'CA'),
('472-27-2349', 'Gringlesby', 'Burt', 'PO Box 792', 'Covelo', 'CA'),
('486-29-1786', 'Locksley', 'Charlene', '18 Broadway Av.', 'San Francisco', 'CA');




INSERT INTO Title (Title_id, title, type, price, Pub_id) VALUES 
('BU1032', 'The Busy Executive\'s Database Guide', 'business', 19.99, '1389'),
('BU1111', 'Cooking with Computers', 'business', 11.95, '1389'),
('BU2075', 'You Can Combat Computer Stress!', 'business', 2.99, '736'),
('BU7832', 'Straight Talk About Computers', 'business', 19.99, '1389'),
('MC2222', 'Silicon Valley Gastronomic Treats', 'mod_cook', 19.99, '877'),
('MC3021', 'The Gourmet Microwave', 'mod_cook', 2.99, '877'),
('MC3026', 'The Psychology of Computer Cooking', 'UNDECIDED', NULL, '877'),
('PC1035', 'But Is It User Friendly?', 'popular_comp', 22.95, '1389'),
('PC8888', 'Secrets of Silicon Valley', 'popular_comp', 20.00, '1389'),
('PC9999', 'Net Etiquette', 'popular_comp', 10.95, '1389'),
('PS2091', 'Is Anger the Enemy?', 'psychology', NULL, '736');




INSERT INTO Publisher (Pub_id, Pub_name, city)
VALUES
  (736, 'New Moon Books', 'Boston'),
  (877, 'Binnet & Hardley', 'Washington'),
  (1389, 'Algodata Infosystems', 'Berkeley'),
  (1622, 'Five Lakes Publishing', 'Chicago'),
  (1756, 'Ramona Publishers', 'Dallas'),
  (9901, 'GGG&G', 'Munchen'),
  (9952, 'Scootney Books', 'New York'),
  (9999, 'Lucerne Publishing', 'Paris');



INSERT INTO Author_title (Au_id, Title_id)
VALUES 
('172-32-1176', 'PS3333'),
('213-46-8915', 'BU1032'),
('213-46-8915', 'BU2075'),
('238-95-7766', 'PC1035'),
('267-41-2394', 'BU1111'),
('267-41-2394', 'TC7777'),
('274-80-9391', 'BU7832'),
('409-56-7008', 'BU1032'),
('427-17-2319', 'PC8888'),
('472-27-2349', 'TC7777');


-- a. List the books Authored by Marjorie Green. --
SELECT Title.title
FROM Author
JOIN Author_title ON Author.Au_id = Author_title.Au_id
JOIN Title ON Author_title.Title_id = Title.Title_id
WHERE Author.Au_fname = 'Marjorie' AND Author.Au_lname = 'Green';

- "The Busy Executives Database Guide"
- "You Can Combat Computer Stress"

-- b. List the books Authored by Michael O'Leary. --
SELECT Title.title
FROM Author_title
JOIN Title ON Author_title.Title_id = Title.Title_id
WHERE Author_title.Au_id = '267-41-2394';

- "Cooking with Computers"

-- c. Write the author/s of "The Busy Executives Database Guide". --
SELECT Author.Au_lname, Author.Au_fname
FROM Author
JOIN Author_title ON Author.Au_id = Author_title.Au_id
JOIN Title ON Title.Title_id = Author_title.Title_id
WHERE Title.title = 'The Busy Executive\'s Database Guide';

- "Marjorie Green"
- "Abraham Bennet"


-- d. Identify the publisher of "But Is It User Friendly?". --
SELECT Pub_name 
FROM Publisher 
INNER JOIN Title ON Publisher.Pub_id = Title.Pub_id 
WHERE Title.title = 'But Is It User Friendly?';

-"Algodata Infosystem"
-- e. List the books published by Algodata Infosystems.--
SELECT title
FROM title
WHERE pub_id = '1389';

- "The Busy Executives Database Guide"
- "Cooking with Computers"
- "Straight Talk About Computers"
- "But is it User Friendly"
- "Secrets of Silicon Valley"
- "Net Etiquette"


-- Create a database for a blog using SQL syntax

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
    FOREIGN KEY(author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
CONSTRAINT fk_user_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_post_id
    FOREIGN KEY(post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
CONSTRAINT fk_user_id
    FOREIGN KEY(user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);